package com.trek.cicd.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CicdController {

    public static final String WELCOME = "Welcome to Trek CICD";

    @RequestMapping("/cicd")
    public String getCicd() {
        return WELCOME;
    }
}
