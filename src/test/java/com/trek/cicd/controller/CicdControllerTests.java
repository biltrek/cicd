package com.trek.cicd.controller;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class CicdControllerTests {

    private CicdController cicdController = new CicdController();

    @Test
    void getCicd() {
        assertThat(cicdController.getCicd()).isEqualTo(CicdController.WELCOME);
    }

}
